# -*- coding: utf-8 -*-
import os
import cv2
import logging
from winUI import RecoderUI, MagUI
from videoMag import ShowFreqs, VideoExtraction, Magnification
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QMainWindow, QFileDialog
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QTimer

logging.basicConfig(level=logging.INFO)

class RecoderWin(QMainWindow, RecoderUI):
    switchWin1 = pyqtSignal()
    def __init__(self):
        super(RecoderWin, self).__init__()
        self.stream = Stream()
        self.stream.start()
        self.stream.imgUpdate.connect(self.imgUpdateSlot)
        self.setupUi(self)
        self.pushBtnRecord.clicked.connect(self.checkFeed)
        self.pushBtnMag.clicked.connect(self.goMag)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.cntDown)
        self.initTime = 20
        self.enableAllBtn = False
        self.reRecord = False
        
    def imgUpdateSlot(self, img):
        self.displayLabel.setPixmap(QPixmap.fromImage(img))
        if self.enableAllBtn is False:
            self.pushBtnRecord.setEnabled(True)
            self.pushBtnMag.setEnabled(True)
            self.enableAllBtn = True

    def checkFeed(self):
        self.stream.imgUpdate.connect(self.imgUpdateSlot)
        self.infoLabel.setText('開始錄影...請維持姿勢20秒')
        self.stream.startRecord()
        self.timer.start(1000)
        self.pushBtnRecord.setText('停止')
        self.pushBtnRecord.clicked.connect(self.cancelFeed)
        self.pushBtnMag.setEnabled(False)

    def cancelFeed(self):
        self.stream.stop()
        self.infoLabel.setText("請將手腕放置指定區域，開始後將錄影20秒")
        self.pushBtnRecord.setText('開始')
        self.pushBtnRecord.clicked.connect(self.checkFeed)
        self.timer.stop()
        self.initTime = 20
        self.counter.display(self.initTime)
        self.pushBtnMag.setEnabled(True)

    def cntDown(self):
        if self.initTime < 10:
            curTime = '0' + str(self.initTime)
        else: 
            curTime = str(self.initTime)
        self.counter.display(curTime)
        self.initTime -= 1
        if self.initTime < 0:
            self.cancelFeed()

    def goMag(self):
        self.switchWin1.emit()
        logging.info(' The camera is closed.')
        self.stream.exit()
        self.reRecord = True
        self.displayLabel.setPixmap(QPixmap(""))
        self.displayLabel.setText("畫面處理中...")

class Stream(QThread):
    imgUpdate = pyqtSignal(QImage)
    def run(self):
        self.record = False
        self.flag = True
        self.ThreadActive = True
        self.vidCnt = 1
        cap = cv2.VideoCapture(0)
        self.width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        while self.ThreadActive:
            if self.record is True and self.flag is True:
                fourcc = cv2.VideoWriter_fourcc(*'XVID')
                cur_dir = os.listdir()
                while True:
                    tmpVid_name = 'output_{:1d}'.format(self.vidCnt)+'.avi'
                    if tmpVid_name in cur_dir:
                        self.vidCnt += 1
                    else:
                        break 
                self.out = cv2.VideoWriter(tmpVid_name, fourcc, 30.0, (self.width, self.height))
                self.flag = False
            ret, frame = cap.read()
            if ret:
                img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = img.shape
                self._drawLine(img)
                bytesPerLine = ch * w
                convertToQtFormat = QImage(img.data, w, h, bytesPerLine, QImage.Format_RGB888)
                pic = convertToQtFormat.scaled(self.width, self.height, Qt.KeepAspectRatio)
                self.imgUpdate.emit(pic)
            if self.record is True and self.flag is False:
                self._drawSquare(img, 'red')
                self.out.write(frame)
            else:
                self._drawSquare(img, 'green')
        cap.release()
        cv2.destroyAllWindows()

    def _drawLine(self, img):
        thickness = 2
        cv2.line(img, (0, 20), (255, 100), (255, 0, 0), thickness)
        cv2.line(img, (255, 100), (255, 420), (255, 0, 0), thickness)
        cv2.line(img, (0, 430), (255, 420), (255, 0, 0), thickness)

    def _drawSquare(self, img , color):
        thickness = 20
        if color == 'red':
            rgb = (255, 0, 0)
        else:
            rgb = (124, 252, 0)
        cv2.rectangle(img, (0, 0), (self.width, self.height), rgb, thickness)

    def startRecord(self):
        self.record = True
        self.flag = True

    def stop(self):
        self.record = False
        self.out.release()
    
    def exit(self):
        self.record = False
        self.flag = True
        self.ThreadActive = False
        
class MagWin(QMainWindow, MagUI):
    switchWin2 = pyqtSignal()
    def __init__(self):
        super(MagWin, self).__init__()
        self.setupUi(self)
        self.pushBtnLoadFile.clicked.connect(self.loadFile)
        self.pushBtnCon.clicked.connect(self.infoConfirm)
        self.pushBtnClear.clicked.connect(self.infoClear)
        self.pushBtnBack.clicked.connect(self.goBack)
        self.hyperParam_dict = {'vidPath': None, 'ampFactor': None, 'lambdaC': None,'highCutoff': None, 
                                'lowCutoff': None, 'sampleRate': None, 'chromAtten': None}    

        self.roi = [100, 150, 100, 150]
    
    def loadFile(self):
        videoName, filetype = QFileDialog.getOpenFileName(self,
                  "選取檔案", "./", "Video Files (*.avi)")
        self.lineEditLoadFile.setText(videoName)

    def infoConfirm(self):
        self.disable()
        self.labelHR_2.setText("- -")
        self.labelRGB.setPixmap(QPixmap(""))
        hyperParam = [self.lineEditLoadFile.text(), self.lineEditAF.text(),
                      self.lineEditLC.text(), self.lineEditHighCutoff.text(),
                      self.lineEditLowCutoff.text(), self.lineEditSR.text(),
                      self.doubleSpinBoxCA.text()]
        
        local_hyperParam_dict = self.hyperParam_dict
        if os.path.isfile(hyperParam[0]):
            try:
                local_hyperParam_dict['vidPath'] = hyperParam[0]
                local_hyperParam_dict['ampFactor'] = float(hyperParam[1])
                local_hyperParam_dict['lambdaC'] = float(hyperParam[2])
                local_hyperParam_dict['highCutoff'] = float(hyperParam[3])
                local_hyperParam_dict['lowCutoff'] = float(hyperParam[4])
                if local_hyperParam_dict['highCutoff'] <= local_hyperParam_dict['lowCutoff']:      
                    self.labelRE.setText('提示：High cutoff 必須大於 Low cutoff')
                    self.enable()
                else:
                    local_hyperParam_dict['sampleRate'] = float(hyperParam[5])
                    local_hyperParam_dict['chromAtten'] = float(hyperParam[6])
                    self.labelRGB.setText('影像處理中...')
                    self.magVid(local_hyperParam_dict['vidPath'],
                                local_hyperParam_dict['ampFactor'],
                                local_hyperParam_dict['lambdaC'],
                                local_hyperParam_dict['highCutoff'],
                                local_hyperParam_dict['lowCutoff'],
                                local_hyperParam_dict['sampleRate'],
                                local_hyperParam_dict['chromAtten']
                                ) 
                    
                    self.labelRE.setText("")
                    magnifiedVid = self.findMagVid(local_hyperParam_dict['vidPath'])
                    self.getFreq(magnifiedVid, self.roi, self.checkboxDisplay.isChecked())
            except:
                self.labelRE.setText('提示：請輸入所有參數')
                self.enable()
        else:
            self.labelRE.setText('提示：此檔案不存在!!')
            self.enable()
        
    def infoClear(self):
        self.clear()

    def goBack(self):
        self.clear()
        self.switchWin2.emit()

    def clear(self):
        self.labelHR_2.setText("- -")
        self.lineEditLoadFile.setText("")
        self.lineEditLoadFile.setText("")
        self.lineEditAF.setText("")
        self.lineEditSR.setText("30.0")
        self.lineEditHighCutoff.setText("")
        self.lineEditLowCutoff.setText("")
        self.lineEditLC.setText("")
        self.doubleSpinBoxCA.setValue(0)
        self.checkboxDisplay.setChecked(False)
        self.labelRE.setText("")
        self.labelRGB.setPixmap(QPixmap(""))
        self.labelRGB.setText("準備中...")

    def disable(self):
        self.pushBtnCon.setEnabled(False)
        self.pushBtnClear.setEnabled(False)
        self.pushBtnBack.setEnabled(False)

    def enable(self):
        self.pushBtnCon.setEnabled(True)
        self.pushBtnClear.setEnabled(True)
        self.pushBtnBack.setEnabled(True)

    def magVid(self, vidFile, alpha, lambda_c, fh, fl, sampleRate, chromAttenuation):
        outDir = './'
        logging.info(' Starting to magnify color...')
        mag = Magnification(vidFile, outDir, alpha, lambda_c,
                            fh, fl, sampleRate, chromAttenuation)
        mag.startMag()
        logging.info(' Generated')

    def getFreq(self, vid_path, roi, vid_dis, max_iter=3000,
                freq_mode='ica', save_name=None):
        logging.info(' Starting to analyze pulse frequency...')
        VE = VideoExtraction(vid_path, roi, vid_dis)
        fps, vid_len = VE.get_fps, VE.get_vid_len
        all_avg = VE.video_run()
        show_result = ShowFreqs(fps, vid_len, all_avg, max_iter, freq_mode, save_name)
        result, heart_rate = show_result.plot()

        size = (int(result.shape[1]*0.7), int(result.shape[0]*0.7))
        re_result = cv2.resize(result, size)
        h, w, ch = re_result.shape
        bytesPerLine = ch * w
        q_result = QImage(re_result.data, w, h, bytesPerLine, QImage.Format_RGB888)

        self.labelRGB.setPixmap(QPixmap.fromImage(q_result))
        self.labelHR_2.setText(str(heart_rate))
        self.enable()

    def findMagVid(self, magnifiedVid):
        magnifiedVid_split = os.path.split(magnifiedVid)
        magnifiedVid_name = magnifiedVid_split[-1]
        target_name = magnifiedVid_name.split('.')[0] + '-ideal-from-'
        for file in os.listdir(os.curdir):
            if target_name in file:
                target = file
                break
        return os.path.join(magnifiedVid_split[0], target)

class Controller():
    def __init__(self):
        self.recoder = RecoderWin()
        self.mag = MagWin()

    def showRecoder(self):
        self.mag.close()
        self.recoder.switchWin1.connect(self.showMag)
        self.recoder.show()
        if self.recoder.reRecord is True:
            self.recoder.stream.start()
            self.recoder.reRecord = False

    def showMag(self):
        self.recoder.close()
        self.mag.switchWin2.connect(self.showRecoder)
        self.mag.show()