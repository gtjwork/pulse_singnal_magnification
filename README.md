<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" type="image/png" href="https://hackmd.io/favicon.png">
    <link rel="apple-touch-icon" href="https://hackmd.io/apple-touch-icon.png">
</head>

<body>
    <div id="doc" class="markdown-body container-fluid comment-enabled" data-hard-breaks="true" style=""><h1 id="影像式脈搏訊號量測與數據採集系統" data-id="影像式脈搏訊號量測與數據採集系統" style=""><a class="anchor hidden-xs" href="#影像式脈搏訊號量測與數據採集系統" title="影像式脈搏訊號量測與數據採集系統"><span class="octicon octicon-link"></span></a><span>影像式脈搏訊號量測與數據採集系統</span></h1><ul>
<li><small><i class="fa fa-user"></i> 作者：Jeff</small></li>
<li><small><i class="fa fa-clock-o"></i> Jul, Thu 1, 2021 18:45</small></li>
</ul><h2 id="目錄" data-id="目錄" style=""><a class="anchor hidden-xs" href="#目錄" title="目錄"><span class="octicon octicon-link"></span></a><span>目錄</span></h2><p><span class="toc"><ul>
<li><a href="#影像式脈搏訊號量測與數據採集系統" title="影像式脈搏訊號量測與數據採集系統">影像式脈搏訊號量測與數據採集系統</a><ul>
<li><a href="#目錄" title="目錄">目錄</a></li>
<li><a href="#環境設置" title="環境設置">環境設置</a><ul>
<li><a href="#Python-環境" title="Python 環境">Python 環境</a></li>
<li><a href="#Matlab-環境" title="Matlab 環境">Matlab 環境</a></li>
</ul>
</li>
<li><a href="#介面操作" title="介面操作">介面操作</a><ul>
<li><a href="#錄影頁面" title="錄影頁面">錄影頁面</a></li>
<li><a href="#影像處理畫面" title="影像處理畫面">影像處理畫面</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</span></p><h2 id="環境設置" data-id="環境設置" style=""><a class="anchor hidden-xs" href="#環境設置" title="環境設置"><span class="octicon octicon-link"></span></a><span>環境設置</span></h2><h3 id="Python-環境" data-id="Python-環境" style=""><a class="anchor hidden-xs" href="#Python-環境" title="Python-環境"><span class="octicon octicon-link"></span></a><span>Python 環境</span></h3><h4 id="1-安裝-Anaconda" data-id="1-安裝-Anaconda"><a class="anchor hidden-xs" href="#1-安裝-Anaconda" title="1-安裝-Anaconda"><span class="octicon octicon-link"></span></a><span>[1] 安裝 </span><a href="https://www.anaconda.com/products/individual" target="_blank" rel="noopener"><span>Anaconda</span></a></h4><h4 id="2-建立虛擬環境" data-id="2-建立虛擬環境"><a class="anchor hidden-xs" href="#2-建立虛擬環境" title="2-建立虛擬環境"><span class="octicon octicon-link"></span></a><span>[2] 建立虛擬環境</span></h4><pre><code>conda create -n env_name python=3.7
activate env_name
pip install opencv-python              (3.4.2)
pip install numpy                      (1.20.1)
pip install matplotlib                 (3.4.1)
pip install PyQt5                      (5.15.4)
pip install pyqt5-tools                (5.15.4.3.2)
conda install -c anaconda scikit-learn (0.23.2)
pip install scipy                      (1.6.2)
</code></pre><h3 id="Matlab-環境" data-id="Matlab-環境" style=""><a class="anchor hidden-xs" href="#Matlab-環境" title="Matlab-環境"><span class="octicon octicon-link"></span></a><span>Matlab 環境</span></h3><h4 id="1-安裝-Matlab。" data-id="1-安裝-Matlab。"><a class="anchor hidden-xs" href="#1-安裝-Matlab。" title="1-安裝-Matlab。"><span class="octicon octicon-link"></span></a><span>[1] 安裝 </span><a href="https://www.mathworks.com/products/matlab.html" target="_blank" rel="noopener"><span>Matlab</span></a><span>。</span></h4><h4 id="2-Python-中調用-matlab-檔案" data-id="2-Python-中調用-matlab-檔案"><a class="anchor hidden-xs" href="#2-Python-中調用-matlab-檔案" title="2-Python-中調用-matlab-檔案"><span class="octicon octicon-link"></span></a><span>[2] Python 中調用 matlab 檔案</span></h4><ol>
<li><span>檢查是否有設定 Matlab 環境變數。</span></li>
<li><span>使用系統管理員權限開啟終端，並進入 python 虛擬環境，執行以下指令：</span><pre><code>cd path to\MATLAB\R2018b\extern\engines\python
python setup.py install
</code></pre>
</li>
<li><span>將資料夾 </span><code>build</code><span> 複製到虛擬環境的程式庫中，即可在該環境中使用 matlab 指令和檔案：</span><pre><code>C:\Users\username\anaconda3\envs\evm\Lib
</code></pre>
</li>
</ol><h2 id="介面操作" data-id="介面操作" style=""><a class="anchor hidden-xs" href="#介面操作" title="介面操作"><span class="octicon octicon-link"></span></a><span>介面操作</span></h2><h3 id="錄影頁面" data-id="錄影頁面" style=""><a class="anchor hidden-xs" href="#錄影頁面" title="錄影頁面"><span class="octicon octicon-link"></span></a><span>錄影頁面</span></h3><h4 id="1-開始錄影" data-id="1-開始錄影"><a class="anchor hidden-xs" href="#1-開始錄影" title="1-開始錄影"><span class="octicon octicon-link"></span></a><span>[1] 開始錄影</span></h4><ul>
<li><span>手掌方向符合紅色框，手腕相對位置對準粉色標籤位置。</span><br>
<img src="https://i.imgur.com/a5qw8Iq.png" alt="" width="480" loading="lazy"></li>
<li><span>點選開始，計時器倒數20秒。</span><br>
<img src="https://i.imgur.com/t1dD6zr.png" alt="" width="480" loading="lazy"></li>
<li><span>錄製中畫面。點選停止與完成20秒將回到原先的畫面。</span><br>
<img src="https://i.imgur.com/w9nSfvP.png" alt="" width="480" loading="lazy"></li>
<li><span>點選影像處理，將會跳轉至影像處理畫面。</span><br>
<img src="https://i.imgur.com/JdEt3Yz.png" alt="" width="480" loading="lazy"></li>
</ul><h3 id="影像處理畫面" data-id="影像處理畫面" style=""><a class="anchor hidden-xs" href="#影像處理畫面" title="影像處理畫面"><span class="octicon octicon-link"></span></a><span>影像處理畫面</span></h3><ul>
<li><span>跳轉後初始畫面。</span><br>
<img src="https://i.imgur.com/GfxU2Pu.png" alt="" width="480" loading="lazy"></li>
<li><span>輸入參數和選項。</span><br>
<img src="https://i.imgur.com/khm4v67.png" alt="" width="480" loading="lazy"></li>
<li><span>若有選擇</span><strong><span>影像放大</span></strong><span>，影片處理完將會自動撥放處理後影片。</span><br>
<img src="https://i.imgur.com/DJCIzyV.png" alt="" width="480" loading="lazy"></li>
<li><span>結果顯示。上圖為亮度變化，下圖為所存在之可能頻率。</span><br>
<img src="https://i.imgur.com/LvR3yqY.png" alt="" width="480" loading="lazy"></li>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8=" crossorigin="anonymous" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gist-embed/2.6.0/gist-embed.min.js" integrity="sha256-KyF2D6xPIJUW5sUDSs93vWyZm+1RzIpKCexxElmxl8g=" crossorigin="anonymous" defer></script>
<script>
	var markdown = $(".markdown-body");
	//smooth all hash trigger scrolling
	function smoothHashScroll() {
		var hashElements = $("a[href^='#']").toArray();
		for (var i = 0; i < hashElements.length; i++) {
			var element = hashElements[i];
			var $element = $(element);
			var hash = element.hash;
			if (hash) {
				$element.on('click', function (e) {
					// store hash
					var hash = this.hash;
					if ($(hash).length <= 0) return;
					// prevent default anchor click behavior
					e.preventDefault();
					// animate
					$('body, html').stop(true, true).animate({
						scrollTop: $(hash).offset().top
					}, 100, "linear", function () {
						// when done, add hash to url
						// (default click behaviour)
						window.location.hash = hash;
					});
				});
			}
		}
	}

	smoothHashScroll();
	var toc = $('.ui-toc');
	var tocAffix = $('.ui-affix-toc');
	var tocDropdown = $('.ui-toc-dropdown');
	//toc
	tocDropdown.click(function (e) {
		e.stopPropagation();
	});

	var enoughForAffixToc = true;

	function generateScrollspy() {
		$(document.body).scrollspy({
			target: ''
		});
		$(document.body).scrollspy('refresh');
		if (enoughForAffixToc) {
			toc.hide();
			tocAffix.show();
		} else {
			tocAffix.hide();
			toc.show();
		}
		$(document.body).scroll();
	}

	function windowResize() {
		//toc right
		var paddingRight = parseFloat(markdown.css('padding-right'));
		var right = ($(window).width() - (markdown.offset().left + markdown.outerWidth() - paddingRight));
		toc.css('right', right + 'px');
		//affix toc left
		var newbool;
		var rightMargin = (markdown.parent().outerWidth() - markdown.outerWidth()) / 2;
		//for ipad or wider device
		if (rightMargin >= 133) {
			newbool = true;
			var affixLeftMargin = (tocAffix.outerWidth() - tocAffix.width()) / 2;
			var left = markdown.offset().left + markdown.outerWidth() - affixLeftMargin;
			tocAffix.css('left', left + 'px');
		} else {
			newbool = false;
		}
		if (newbool != enoughForAffixToc) {
			enoughForAffixToc = newbool;
			generateScrollspy();
		}
	}
	$(window).resize(function () {
		windowResize();
	});
	$(document).ready(function () {
		windowResize();
		generateScrollspy();
	});

	//remove hash
	function removeHash() {
		window.location.hash = '';
	}

	var backtotop = $('.back-to-top');
	var gotobottom = $('.go-to-bottom');

	backtotop.click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (scrollToTop)
			scrollToTop();
		removeHash();
	});
	gotobottom.click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (scrollToBottom)
			scrollToBottom();
		removeHash();
	});

	var toggle = $('.expand-toggle');
	var tocExpand = false;

	checkExpandToggle();
	toggle.click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		tocExpand = !tocExpand;
		checkExpandToggle();
	})

	function checkExpandToggle () {
		var toc = $('.ui-toc-dropdown .toc');
		var toggle = $('.expand-toggle');
		if (!tocExpand) {
			toc.removeClass('expand');
			toggle.text('Expand all');
		} else {
			toc.addClass('expand');
			toggle.text('Collapse all');
		}
	}

	function scrollToTop() {
		$('body, html').stop(true, true).animate({
			scrollTop: 0
		}, 100, "linear");
	}

	function scrollToBottom() {
		$('body, html').stop(true, true).animate({
			scrollTop: $(document.body)[0].scrollHeight
		}, 100, "linear");
	}
</script>
</body>

</html>
