import sys
import os
import sys
import logging
from allWin import Controller
from PyQt5.QtWidgets import QApplication

logging.basicConfig(level=logging.INFO)

def window():
    logging.info(' Initialize app...')
    app = QApplication(sys.argv)
    controller = Controller()
    logging.info(' Opening the app')
    controller.showRecoder()
    sys.exit(app.exec_())

if __name__ == "__main__":
    window()