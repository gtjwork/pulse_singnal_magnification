import os 
import cv2
import logging
import numpy as np
import scipy.fftpack
import matlab.engine as mlab
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA

logging.basicConfig(level=logging.INFO)

class RgbFreq():
    def __init__(self, fps, vid_len, roi_avg):
        self.fps = fps
        self.vid_len = vid_len
        self.roi_avg = roi_avg
        self.idx, self.freqs = self._get_freqs(roi_avg[0].shape[0])
        self.charts_x = 1
        self.charts_y = 2
        self.hspace = 0.7
        self.figsize = (15, 8)

    def _get_freqs(self, roi_avg_shape):
        freqs = scipy.fftpack.fftfreq(roi_avg_shape, d=1.0/self.fps)
        idx = np.argsort(freqs)
        freqs = freqs[idx]
        freqs = freqs[len(freqs)//2+1 :]
        return idx, freqs

    def _fft(self, roi_avg):
        color_fft = abs(scipy.fftpack.fft(roi_avg))
        color_fft = color_fft[self.idx]
        color_fft = color_fft[len(self.idx)//2+1:]
        return color_fft

    def _channel_selector(self, raw_signal):
        max_amp, max_freq = [], []
        for i in range(len(raw_signal)):
            find_freqs = abs(self._fft(raw_signal[i]))
            freq_selected = find_freqs[np.where(np.logical_and(self.freqs >= 0.9, self.freqs <= 2.1))]
            max_amp.append(max(freq_selected))
            max_freq.append(self.freqs[np.where(find_freqs == max(freq_selected))][0])
        idx_selector = max_amp.index(max(max_amp))
        return idx_selector, max_freq[idx_selector]

    def _rgb_plot(self):
        rgb_words = ['Blue', 'Green', 'Red']
        rgb_idx, max_freq = self._channel_selector(self.roi_avg)
        freq_min = round(max_freq*60)

        fig = plt.figure('RGB mode', self.figsize)
        plt.subplots_adjust(hspace=self.hspace)
        plt.subplot(self.charts_y, self.charts_x, 1)
        plt.title('Pixel average - {} channel'.format(rgb_words[rgb_idx]))
        plt.xlabel('Time')
        plt.ylabel('Brightness')
        plt.plot(self.roi_avg[0])

        plt.subplot(self.charts_y, self.charts_x, 2)
        plt.title('FFT - {} channel'.format(rgb_words[rgb_idx]))
        plt.xlabel('Freq (Hz)')
        plt.plot(self.freqs, abs(self._fft(self.roi_avg[rgb_idx])))
        # fig.text(0.65, 0.35, 'Heat Rate: {:.0f} per min'.format(freq_min),\
        #          color='r', fontsize=18)
        fig.canvas.draw()
        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        fig.clear()
        return data, freq_min

class ICAFreq(RgbFreq):
    def __init__(self, fps, vid_len, roi_avg, max_iter):
        super().__init__(fps, vid_len, roi_avg)
        self.max_iter = max_iter
        self.all_avg = np.c_[self.roi_avg[0], \
                        self.roi_avg[1], self.roi_avg[2]]

    def _ICA_process(self):
        ica = FastICA(n_components=3, max_iter=self.max_iter)
        ica_dataset = ica.fit_transform(self.all_avg)
        ica_dataset = [ica_dataset[:,ch] for ch in range(ica_dataset.shape[1])]
        return ica_dataset

    def _ica_plot(self):
        ica_words = ['CH1', 'CH2', 'CH3']
        ica_dataset = self._ICA_process()
        ica_idx, max_freq = self._channel_selector(ica_dataset)
        freq_min = round(max_freq*60)

        fig = plt.figure('ICA mode', self.figsize)
        plt.subplots_adjust(hspace=self.hspace)
        plt.subplot(self.charts_y, self.charts_x, 1)
        plt.title('Pixel average - {}'.format(ica_words[ica_idx]))
        plt.xlabel('Time')
        plt.ylabel('Brightness')
        plt.plot(ica_dataset[ica_idx])

        plt.subplot(self.charts_y, self.charts_x, 2)
        plt.title('FFT - {}'.format(ica_words[ica_idx]))
        plt.xlabel('Freq (Hz)')
        plt.plot(self.freqs, abs(self._fft(ica_dataset[ica_idx])))
        # fig.text(0.65, 0.35, 'Heat Rate: {:.0f} per min'.format(freq_min),\
        #          color='r', fontsize=18)
        fig.canvas.draw()
        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        fig.clear()
        return data, freq_min

class ShowFreqs(ICAFreq):
    def __init__(self, fps, vid_len, roi_avg, max_iter, freq_mode, save_name):
        super().__init__(fps, vid_len, roi_avg, max_iter)
        self.freq_mode = freq_mode
        self.save_name = save_name

    def plot(self):
        if self.freq_mode == 'rgb':
            logging.info(' RGB frequency mode')
            result, heart_rate = self._rgb_plot()
            if self.save_name is not None:
                plt.savefig(os.path.join(os.getcwd(), self.save_name + '_rgb.png'))
        elif self.freq_mode == 'ica':
            logging.info(' ICA frequency mode')
            result, heart_rate = self._ica_plot()
            if self.save_name is not None:
                plt.savefig(os.path.join(os.getcwd(), self.save_name + '_ica.png'))
        elif self.freq_mode == 'both':
            logging.info(' RGB and ICA frequency mode')
            result, heart_rate = self._rgb_plot()
            if self.save_name is not None:
                plt.savefig(os.path.join(os.getcwd(), self.save_name + '_rgb.png'))
            result, heart_rate = self._ica_plot()
            if self.save_name is not None:
                plt.savefig(os.path.join(os.getcwd(), self.save_name + '_ica.png'))
        else:
            raise ValueError('Please select a freqency mode. (rgb, ica or both)')
        logging.info(' Showing figure...')
        #import ipdb;ipdb.set_trace()
        #plt.show()
        logging.info(' Finished')
        return result, heart_rate

class VideoExtraction():
    def __init__(self, path, roi, vid_dis):
        self.vid = cv2.VideoCapture(path)
        self.roi = roi
        self.vid_dis = vid_dis

    @property
    def get_fps(self):
        return self.vid.get(cv2.CAP_PROP_FPS)

    @property
    def get_vid_len(self):
        return int(self.vid.get(cv2.CAP_PROP_FRAME_COUNT))

    def _video_display(self):
        if type(self.vid_dis) is bool:
            return self.vid_dis
        else:
            if (self.vid_dis == 'y') or (self.vid_dis == 'Y'):
                output = True
            elif (self.vid_dis == 'n') or (self.vid_dis == 'N'):
                output = False
            else:
                raise ValueError('Please choose y or n.')
            return output
 
    def _video_processing(self):
        ppix_x1, ppix_x2, ppix_y1, ppix_y2 = self.roi

        if (ppix_x1 >= ppix_x2) or (ppix_y1 >= ppix_y2):
            raise ValueError('The parameters, y and x, have to be different.')

        vid_len = self.get_vid_len
        roi_region = np.zeros((vid_len, ppix_y2 - ppix_y1, ppix_x2 - ppix_x1, 3))
        all_avg, b_avg, g_avg, r_avg = np.zeros((vid_len)), np.zeros((vid_len)), \
                                       np.zeros((vid_len)), np.zeros((vid_len))
        vid_display = self._video_display()
        cnt = 0
        logging.info(' Video process starting...')
        if vid_display is True:
            logging.info(' Showing video...')
        while self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret is True:
                tmp_roi_region = frame[ppix_y1:ppix_y2, ppix_x1:ppix_x2].squeeze()
                roi_region[cnt,:,:,:] = tmp_roi_region
                b_frame, g_frame, r_frame = cv2.split(tmp_roi_region)
                b_avg[cnt], g_avg[cnt], r_avg[cnt] = \
                    np.mean(b_frame), np.mean(g_frame), np.mean(r_frame)
            else: 
                break
            if vid_display is True:
                cv2.line(frame, (ppix_x1, ppix_y1), (ppix_x2, ppix_y2), (0, 0, 255), 2)
                cv2.imshow('RoI Demo', frame)
                if cv2.waitKey(36) & 0xFF == ord('q'): 
                    raise ValueError('Video interrupting...')
            cnt += 1
        logging.info(' Video end')
        self.vid.release()
        cv2.destroyAllWindows()
        return [b_avg, g_avg, r_avg]

    def _standardization(self, com_avg):
        out = []
        for i in range(len(com_avg)):
            tmp_avg = (com_avg[i] - np.mean(com_avg[i])) / np.std(com_avg[i])
            out.append(tmp_avg)
        return out

    def video_run(self):
        com_avg = self._video_processing()
        all_avg = self._standardization(com_avg)
        return all_avg

class Magnification():
    def __init__(self, vidFile, outDir, alpha, lambda_c,
                 fh, fl, sampleRate, chromAttenuation):
        self.vidFile = str(vidFile)
        self.outDir = str(outDir)
        self.alpha = float(alpha)
        self.lambda_c = float(lambda_c)
        self.fh = float(fh)
        self.fl = float(fl)
        self.sampleRate = float(sampleRate)
        self.chromAttenuation = float(chromAttenuation)
        self.eng = mlab.start_matlab()

        matPath = os.path.abspath(os.getcwd())
        self.eng.addpath(matPath+'\\matlabPyrTools')
        self.eng.addpath(matPath+'\\matlabPyrTools\\MEX')

    def startMag(self):
        _ = self.eng.amplify_spatial_lpyr_temporal_ideal(self.vidFile, self.outDir ,self.alpha, self.lambda_c,
                                                         self.fl, self.fh, self.sampleRate, self.chromAttenuation)